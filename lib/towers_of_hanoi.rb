# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_accessor :winner
  attr_reader :towers

  def initialize(number_of_discs=3, number_of_towers=3)
    @towers = make_towers(number_of_towers, number_of_discs)
    @counter = 0
  end

  def make_towers(number_of_towers, number_of_discs)
    if number_of_towers < 3
      raise "Not Enough Towers"
    end
    towers = []
    number_of_towers.times { towers << [] }
    towers[0] = make_discs(number_of_discs)
    towers
  end

  def make_discs(number_of_discs)
    if number_of_discs < 1
      raise "Not Enough Discs"
    else
      @winner = (1..number_of_discs).to_a.reverse
    end
  end

  def play
    puts "The goal of the game is to move the discs from one tower to another. Each number represents the discs. The larger the number the larger the disc. A larger disc my not be placed on a smaler disc. Here are your current towers."
    self.render
    while !self.won?
      next_step = false
      until next_step
        from_tower, to_tower = gets_move
        next_step = valid_move?(from_tower, to_tower)
        if next_step == false
          puts "Invalid Move, try agian"
        else
          self.move(from_tower, to_tower)
        end
      end
      if won?
        puts "Congratulations, You've won!"
        puts "It took you #{@counter} moves!"
      else
        self.render
      end
    end

  end

  def gets_move
    puts "Enter the tower number you'd like to remove a disk. Then enter the tower number you'd like to place that disk on. Please seperate you commands by a comma. exp 1, 2 "
    gets.strip.split("").join("").split(",").map{ |el| el.to_i - 1 }
  end

  def valid_move?(from_tower, to_tower)
    from  = @towers[from_tower]
    to = @towers[to_tower]
    return false if from == to
    return false if from.empty?
    return false if !to.empty? && from.last > to.last
    range = (0...@towers.count)
    [from_tower, to_tower].all? { |tower| range.include?(tower) }
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
    @counter += 1
  end



  def render
    idx = 0
    while idx < @towers.count
      puts "Tower#{idx + 1}: #{@towers[idx]}"
      idx += 1
    end
  end

  def won?
    @towers[1..-1].any? { |tower| tower == @winner }
  end

end

if $PROGRAM_NAME == __FILE__
  TowersOfHanoi.new.play
end
